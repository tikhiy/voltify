import { Field, InputType } from 'type-graphql';

@InputType()
export default class CreateUserInput {
  @Field()
  username: string;

  @Field()
  password: string;
}
