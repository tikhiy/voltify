import { Field, InputType } from 'type-graphql';

@InputType()
export default class UpdateUserInput {
  @Field({ nullable: true })
  username?: string;

  @Field({ nullable: true })
  password?: string;
}
