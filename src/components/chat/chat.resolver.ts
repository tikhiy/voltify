import { Arg, ID, Mutation, Query, Resolver } from 'type-graphql';
import CreateChatInput from '../create-chat.input';
import UpdateChatInput from '../update-chat.input';
import Chat from './chat.entity';

@Resolver(() => Chat)
export default class ChatResolver {
  @Query(() => Chat)
  GetChat(@Arg('id', () => ID) id: Chat['id']): Promise<Chat> {
    return Chat.findOne(id);
  }

  @Query(() => [Chat])
  GetChats(): Promise<Chat[]> {
    return Chat.find();
  }

  @Mutation(() => Chat)
  CreateChat(@Arg('chat') chat: CreateChatInput): Promise<Chat> {
    const createdChat = Chat.create({
      messages: [],
      ...chat,
    });

    return createdChat.save();
  }

  @Mutation(() => Chat)
  async UpdateChat(
    @Arg('id', () => ID) id: Chat['id'],
    @Arg('chat') chat: UpdateChatInput,
  ): Promise<Chat> {
    const foundChat = await Chat.findOne(id);
    Object.assign(foundChat, chat);
    return foundChat.save();
  }

  @Mutation(() => Chat)
  DeleteChat(@Arg('id', () => ID) id: string): Promise<unknown> {
    return Chat.findOne(id).then((foundChat) => foundChat.remove());
  }
}
