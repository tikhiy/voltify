type IChat = {
  id: string;
  name: string;
  participants: IUser[];
  messages: IMessage[];
};
