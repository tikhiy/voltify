import { Field, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Message from '../message.entity';
import User from '../user/user.entity';

@Entity()
@ObjectType()
export default class Chat extends BaseEntity implements IChat {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: string;

  @Column()
  @Field()
  name: string;

  @ManyToMany(() => User, ({ chats }) => chats)
  @JoinTable()
  @Field(() => [User])
  participants: IUser[];

  @OneToMany(() => Message, ({ chat }) => chat, { cascade: true })
  @Field(() => [Message])
  messages: IMessage[];
}
