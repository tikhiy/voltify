import { Field, InputType } from 'type-graphql';

@InputType()
export default class UpdateChatInput implements Partial<IChat> {
  @Field({ nullable: true })
  name?: string;
}
