import { Field, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Chat from './chat/chat.entity';
import User from './user/user.entity';

@Entity()
@ObjectType()
export default class Message extends BaseEntity implements IMessage {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: string;

  @Column()
  @Field()
  content: string;

  @ManyToOne(() => User)
  @Field(() => User)
  author: IUser;

  @ManyToOne(() => Chat)
  @Field(() => Chat)
  chat: IChat;
}
