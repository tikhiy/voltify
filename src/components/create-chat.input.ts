import { Field, InputType } from 'type-graphql';

@InputType()
export default class CreateChatInput implements Partial<IChat> {
  @Field()
  name: string;
}
