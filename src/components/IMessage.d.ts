type IMessage = {
  id: string;
  content: string;
  author: IUser;
  chat: IChat;
};
