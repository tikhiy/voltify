import { Field, ID, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Chat from '../chat/chat.entity';
import Message from '../message.entity';

@Entity()
@ObjectType()
export default class User extends BaseEntity implements IUser {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: string;

  @Column()
  @Field()
  username: string;

  @Column()
  @Field()
  password: string;

  @ManyToMany(() => User, ({ following }) => following)
  @JoinTable()
  @Field(() => [User])
  followers: IUser[];

  @ManyToMany(() => User, ({ followers }) => followers)
  @Field(() => [User])
  following: IUser[];

  @OneToMany(() => Message, ({ author }) => author)
  messages: IMessage[];

  @Column()
  @Field()
  online: boolean;

  @ManyToMany(() => Chat, ({ participants }) => participants)
  @JoinTable()
  @Field(() => [Chat], { defaultValue: () => [] as IChat[] })
  chats: IChat[];
}
