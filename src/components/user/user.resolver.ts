import { Arg, ID, Mutation, Query, Resolver } from 'type-graphql';
import { FindManyOptions } from 'typeorm';
import CreateUserInput from '../create-user.input';
import UpdateUserInput from '../update-user.input';
import User from './user.entity';

@Resolver(() => User)
export default class UserResolver {
  @Query(() => User)
  GetUser(@Arg('id', () => ID) id: User['id']): Promise<User> {
    return User.findOne(id);
  }

  @Query(() => [User])
  GetUsers(
    @Arg('orderBy', () => String, { nullable: true })
    orderBy?: keyof Pick<User, 'username' | 'online'>,
  ): Promise<User[]> {
    const options: FindManyOptions<User> = {};

    if (typeof orderBy !== 'undefined') {
      options.order = {
        [orderBy]: 'ASC',
      };
    }

    return User.find(options);
  }

  @Mutation(() => User)
  CreateUser(@Arg('user') user: CreateUserInput): Promise<User> {
    const createdUser = User.create({
      followers: [],
      following: [],
      messages: [],
      online: false,
      chats: [],
      ...user,
    });

    return createdUser.save();
  }

  @Mutation(() => User)
  async UpdateUser(
    @Arg('id', () => ID) id: User['id'],
    @Arg('user') user: UpdateUserInput,
  ): Promise<User> {
    const foundUser = await User.findOne(id);
    Object.assign(foundUser, user);
    return foundUser.save();
  }

  @Mutation(() => User)
  DeleteUser(@Arg('id', () => ID) id: string): Promise<unknown> {
    return User.findOne(id).then((foundUser) => foundUser.remove());
  }
}
