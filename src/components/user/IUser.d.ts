type IUser = {
  id: string;
  username: string;
  password: string;
  followers: IUser[];
  following: IUser[];
  messages: IMessage[];
  online: boolean;
  chats: IChat[];
};
