// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-micro';
import { NextApiHandler } from 'next';
import ChatResolver from 'src/components/chat/chat.resolver';
import CreateChatInput from 'src/components/create-chat.input';
import UpdateChatInput from 'src/components/update-chat.input';
import UserResolver from 'src/components/user/user.resolver';
import { buildSchema } from 'type-graphql';
import { createConnection, getConnection } from 'typeorm';
import { PostgresConnectionCredentialsOptions } from 'typeorm/driver/postgres/PostgresConnectionCredentialsOptions';
import Chat from '../../components/chat/chat.entity';
import CreateUserInput from '../../components/create-user.input';
import Message from '../../components/message.entity';
import UpdateUserInput from '../../components/update-user.input';
import User from '../../components/user/user.entity';

const bootstrap = async (): Promise<NextApiHandler> => {
  const extra: PostgresConnectionCredentialsOptions = {
    ssl: process.env.NODE_ENV === 'production',
  };

  try {
    getConnection();
    await getConnection().close();
  } catch {}

  await createConnection({
    synchronize: JSON.parse(process.env.DATABASE_SYNC),
    entities: [User, Chat, Message],
    extra,
    type: 'postgres',
    url: process.env.DATABASE_URL,
  });

  const schema = await buildSchema({
    resolvers: [UserResolver, ChatResolver],
    orphanedTypes: [
      CreateUserInput,
      UpdateUserInput,
      CreateChatInput,
      UpdateChatInput,
    ],
  });

  return new ApolloServer({
    schema,
  }).createHandler({
    path: '/api/graphql',
  });
};

const handler = bootstrap();

const graphql: NextApiHandler = async (req, res) => {
  return (await handler)(req, res);
};

export const config = {
  api: {
    bodyParser: false,
  },
};

export default graphql;
